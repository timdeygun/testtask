#Accounting System

<u>This is only an API for now. No UI is present.</u>

All accessible <b>end-points</b> were implemented according to the 
"API Recommendations" documentation that you can access using the next link:
https://agileengine.bitbucket.io/fsNDJmGOAwqCpzZx/api

---

Web-Service can be started using both manual and semi-manual ways.

<b>Semi-manual:</b>

To start the server and make a web-service accessible,
please do the following:
1. visit the <b>"executable"</b> directory in the root of the project
2. run the command below using the Command Line: 

java -jar SpringApp-1.0-SNAPSHOT.jar

---

<b>Manual:</b>

Manual way implies the start via the command line.
To do this, please follow the next steps:

1. Visit project root/executable directory;
1. use following (cross-platform as well as JVM) command: <br>
java -jar SpringApp-1.0-SNAPSHOT.jar
