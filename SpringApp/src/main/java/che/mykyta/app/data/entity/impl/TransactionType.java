package che.mykyta.app.data.entity.impl;

/**
 * Represents the type of the transaction.
 *
 * @see che.mykyta.app.service.AccountService
 */
public enum TransactionType {
    
    CREDIT("CREDIT"), DEBIT("DEBIT");
    
    private String description;
    
    TransactionType(String description) {
        this.description = description;
    }
    
    public static TransactionType getTransactionTypeForDescription(String requiredDescription) {
        TransactionType[] values = TransactionType.values();
        for (TransactionType transactionType : values) {
            if (transactionType.getDescription().equals(requiredDescription)) {
                return transactionType;
            }
        }
        return null;
    }
    
    public String getDescription() {
        return description;
    }
}
