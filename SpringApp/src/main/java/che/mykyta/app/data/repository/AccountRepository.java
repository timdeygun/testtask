package che.mykyta.app.data.repository;

import che.mykyta.app.data.entity.AccountEntity;

/**
 * Data type Integer will be used to avoid spending a time
 * to handle any floating numbers issues (only a simulation for debit/credit).
 * <p>
 * Only a simulation.
 */
public interface AccountRepository<T, A extends AccountEntity<T>> {
    
    A getAccount();
    
    Integer getAccountValue();
    
    Integer setAccountValue(Integer value);
    
}
