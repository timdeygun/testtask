package che.mykyta.app.data.mapper;

/**
 * Maps source type to destination type.
 *
 * @param <S> - source type.
 * @param <D> - destination type.
 */
public interface EntityMapper<S, D> {
    
    D map(S source);
}
