package che.mykyta.app.data.entity.impl;

import java.util.Date;
import java.util.Objects;

public class TransactionEntity {
    
    private String id;
    private TransactionType type;
    private Integer amount;
    private Date effectiveDate;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public TransactionType getType() {
        return type;
    }
    
    public void setType(TransactionType type) {
        this.type = type;
    }
    
    public Integer getAmount() {
        return amount;
    }
    
    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    
    public Date getEffectiveDate() {
        return effectiveDate;
    }
    
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionEntity that = (TransactionEntity) o;
        return getId().equals(that.getId()) &&
                getType() == that.getType() &&
                getAmount().equals(that.getAmount()) &&
                getEffectiveDate().equals(that.getEffectiveDate());
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType(), getAmount(), getEffectiveDate());
    }
    
    @Override
    public String toString() {
        return "TransactionEntity{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", amount=" + amount +
                ", effectiveDate=" + effectiveDate +
                '}';
    }
    
}
