package che.mykyta.app.data.entity.impl;

import che.mykyta.app.data.entity.AccountEntity;

import java.util.Objects;

public class IntegerAccountEntity implements AccountEntity<Integer> {
    
    private Integer value;
    
    public IntegerAccountEntity() {
        value = 0;
    }
    
    public Integer getValue() {
        return value;
    }
    
    public void setValue(Integer value) {
        this.value = value;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerAccountEntity that = (IntegerAccountEntity) o;
        return getValue().equals(that.getValue());
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
    
    @Override
    public String toString() {
        return "AccountEntity {" + " value = " + value + '}';
    }
    
}
