package che.mykyta.app.data.mapper.impl;

import che.mykyta.app.data.dto.TransactionDto;
import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.mapper.EntityMapper;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper implements EntityMapper<TransactionEntity, TransactionDto> {
    
    @Override
    public TransactionDto map(TransactionEntity source) {
        if (source != null) {
            TransactionDto transactionDto = new TransactionDto();
            String id = source.getId();
            if (id != null) {
                transactionDto.setId(id);
            }
            transactionDto.setType(source.getType());
            transactionDto.setAmount(source.getAmount());
            transactionDto.setEffectiveDate(source.getEffectiveDate());
            return transactionDto;
        }
        return null;
    }
}
