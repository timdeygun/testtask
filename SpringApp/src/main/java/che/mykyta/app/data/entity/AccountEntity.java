package che.mykyta.app.data.entity;

/**
 * Represents the account entity.
 *
 * @param <T> - the data type that will represent the cash values to work with.
 */
public interface AccountEntity<T> {
    
    T getValue();
    
    void setValue(T value);
}
