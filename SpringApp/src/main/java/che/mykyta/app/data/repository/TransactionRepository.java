package che.mykyta.app.data.repository;

import che.mykyta.app.data.entity.impl.TransactionEntity;

import java.util.List;

public interface TransactionRepository {
    
    List<TransactionEntity> getAllTransactions();
    
    String commitNewTransaction(TransactionEntity transactionEntity);
    
    TransactionEntity getTransactionById(String id);
    
    
}
