package che.mykyta.app.data.repository.db;

import che.mykyta.app.data.entity.AccountEntity;
import che.mykyta.app.data.entity.impl.IntegerAccountEntity;
import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.entity.impl.TransactionType;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Persistence dummy.
 */
@Component
public class DataBaseSim {
    
    private static final int ID_BOUND = 100_000;
    
    //ID Generation simulation
    private Random valueGenerator = new Random();
    
    //Single account.
    private AccountEntity<Integer> accountSim;
    
    //In-memory transaction table (instead of H2).
    private Map<String, TransactionEntity> transactionTableSim;
    
    public DataBaseSim() {
        initDataBase();
    }
    
    private void initDataBase() {
        initAccount();
        initTransactionTable();
    }
    
    private void initTransactionTable() {
        transactionTableSim = new ConcurrentHashMap<>();
        
        for (int i = 0; i < 3; i++) {
            TransactionEntity transactionEntity = new TransactionEntity();
            String newId = generateNewId();
            transactionEntity.setId(newId);
            transactionEntity.setType(TransactionType.DEBIT);
            transactionEntity.setAmount(valueGenerator.nextInt(1_000));
            transactionEntity.setEffectiveDate(new Date());
            
            transactionTableSim.put(newId, transactionEntity);
        }
    }
    
    private void initAccount() {
        accountSim = new IntegerAccountEntity();
        accountSim.setValue(10_000);
    }
    
    public IntegerAccountEntity selectIntegerAccountEntity() {
        return (IntegerAccountEntity) accountSim;
    }
    
    public Collection<TransactionEntity> selectAllTransactions() {
        return transactionTableSim.values();
    }
    
    public String commitNewTransaction(TransactionEntity transactionEntity) {
        String id = transactionEntity.getId();
        transactionTableSim.put(id, transactionEntity);
        return id;
    }
    
    public TransactionEntity selectTransactionById(String id) {
        return transactionTableSim.get(id);
    }
    
    public String generateNewId() {
        return String.valueOf(valueGenerator.nextInt(ID_BOUND));
    }
    
}
