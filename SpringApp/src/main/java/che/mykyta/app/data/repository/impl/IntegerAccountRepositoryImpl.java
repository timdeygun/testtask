package che.mykyta.app.data.repository.impl;

import che.mykyta.app.data.entity.AccountEntity;
import che.mykyta.app.data.entity.impl.IntegerAccountEntity;
import che.mykyta.app.data.repository.AccountRepository;
import che.mykyta.app.data.repository.db.DataBaseSim;
import che.mykyta.app.service.impl.TransactionServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IntegerAccountRepositoryImpl implements AccountRepository<Integer, AccountEntity<Integer>> {
    
    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    
    private DataBaseSim dataBaseSim;
    
    @Autowired
    public IntegerAccountRepositoryImpl(DataBaseSim dataBaseSim) {
        this.dataBaseSim = dataBaseSim;
    }
    
    @Override
    public IntegerAccountEntity getAccount() {
        return dataBaseSim.selectIntegerAccountEntity();
    }
    
    @Override
    public Integer getAccountValue() {
        return dataBaseSim.selectIntegerAccountEntity().getValue();
    }
    
    @Override
    public Integer setAccountValue(Integer value) {
        Integer prevValue = null;
        if (value != null) {
            IntegerAccountEntity accountEntity = dataBaseSim.selectIntegerAccountEntity();
            prevValue = accountEntity.getValue();
            accountEntity.setValue(value);
        }
        return prevValue;
    }
    
}
