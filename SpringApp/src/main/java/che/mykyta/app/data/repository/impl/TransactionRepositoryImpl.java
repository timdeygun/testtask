package che.mykyta.app.data.repository.impl;

import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.repository.TransactionRepository;
import che.mykyta.app.data.repository.db.DataBaseSim;
import che.mykyta.app.service.impl.TransactionServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {
    
    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    
    private DataBaseSim dataBaseSim;
    
    @Autowired
    public TransactionRepositoryImpl(DataBaseSim dataBaseSim) {
        this.dataBaseSim = dataBaseSim;
    }
    
    @Override
    public List<TransactionEntity> getAllTransactions() {
        return new ArrayList<>(dataBaseSim.selectAllTransactions());
    }
    
    @Override
    public String commitNewTransaction(TransactionEntity transactionEntity) {
        if (transactionEntity != null) {
            transactionEntity.setId(dataBaseSim.generateNewId());
            return dataBaseSim.commitNewTransaction(transactionEntity);
        }
        return null;
    }
    
    @Override
    public TransactionEntity getTransactionById(String id) {
        if (id != null) {
            return dataBaseSim.selectTransactionById(id);
        }
        return null;
    }
    
}
