package che.mykyta.app.data.dto;

import che.mykyta.app.data.entity.impl.TransactionType;

import java.util.Date;

public class TransactionDto {
    
    private String id;
    
    private TransactionType type;
    
    private Integer amount;
    
    private Date effectiveDate;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public TransactionType getType() {
        return type;
    }
    
    public void setType(TransactionType type) {
        this.type = type;
    }
    
    public Integer getAmount() {
        return amount;
    }
    
    public void setAmount(Integer amount) {
        this.amount = amount;
    }
    
    public Date getEffectiveDate() {
        return effectiveDate;
    }
    
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
    
}
