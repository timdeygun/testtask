package che.mykyta.app.service;

import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.entity.impl.TransactionType;

import java.util.List;

/**
 * Transaction service according to the 'API Recommendations'.
 */
public interface TransactionService {
    
    List<TransactionEntity> fetchTransactionHistory();
    
    TransactionEntity getTransactionById(String id);
    
    TransactionEntity commitTransaction(TransactionType transactionType, Integer amount);
    
}
