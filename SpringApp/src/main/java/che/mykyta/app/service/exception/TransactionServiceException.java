package che.mykyta.app.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class TransactionServiceException extends RuntimeException {
    
    public TransactionServiceException(String message) {
        super(message);
    }
    
}
