package che.mykyta.app.service;

import che.mykyta.app.data.entity.impl.TransactionType;

/**
 * Let's assume it is a Active Account (debit - REPLENISH, credit - WITHDRAWAL).
 *
 * @param <T> - the data type that will represent the cash.
 */
public interface AccountService<T, V> {
    
    /**
     * Modifies a value on the account it working with.
     *
     * @param value - exact value to operate.
     * @return - previous value before the operation.
     */
    T modifyAccountValue(TransactionType transactionType, V value);
    
}
