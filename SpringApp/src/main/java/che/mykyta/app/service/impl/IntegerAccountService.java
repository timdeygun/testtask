package che.mykyta.app.service.impl;

import che.mykyta.app.data.entity.AccountEntity;
import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.entity.impl.TransactionType;
import che.mykyta.app.data.repository.AccountRepository;
import che.mykyta.app.service.AccountService;
import che.mykyta.app.service.TransactionService;
import che.mykyta.app.service.exception.AccountServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @see che.mykyta.app.service.AccountService
 */
@Service
public class IntegerAccountService implements AccountService<TransactionEntity, Integer> {
    
    private AccountRepository<Integer, AccountEntity<Integer>> accountRepository;
    private TransactionService transactionService;
    
    @Autowired
    public IntegerAccountService(AccountRepository<Integer, AccountEntity<Integer>> accountRepository,
                                 TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
    }
    
    @Override
    public TransactionEntity modifyAccountValue(TransactionType transactionType, Integer value) {
        TransactionEntity transactionEntity = null;
        if (value != null) {
            switch (transactionType) {
                case DEBIT:
                    handleDebit(value);
                    break;
                case CREDIT:
                    handleCredit(value);
                    break;
            }
            transactionEntity = transactionService.commitTransaction(transactionType, value);
        }
        return transactionEntity;
    }
    
    private void handleDebit(int valueToDebit) {
        Integer currentValue = accountRepository.getAccountValue();
        accountRepository.setAccountValue(currentValue + valueToDebit);
    }
    
    private void handleCredit(int valueToCredit) {
        Integer currentValue = accountRepository.getAccountValue();
        if (valueToCredit > currentValue) {
            throw new AccountServiceException("Cannot operate a credit. Not enough funds.");
        }
        accountRepository.setAccountValue(currentValue - valueToCredit);
    }
    
}
