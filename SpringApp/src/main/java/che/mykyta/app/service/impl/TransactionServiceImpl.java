package che.mykyta.app.service.impl;

import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.entity.impl.TransactionType;
import che.mykyta.app.data.repository.TransactionRepository;
import che.mykyta.app.service.TransactionService;
import che.mykyta.app.service.exception.TransactionServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @see che.mykyta.app.service.TransactionService
 */
@Service
public class TransactionServiceImpl implements TransactionService {
    
    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    
    private TransactionRepository transactionRepository;
    
    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }
    
    @Override
    public List<TransactionEntity> fetchTransactionHistory() {
        return transactionRepository.getAllTransactions();
    }
    
    @Override
    public TransactionEntity getTransactionById(String id) {
        if (id != null) {
            return transactionRepository.getTransactionById(id);
        }
        String message = "Transaction ID cannot be null.";
        invokeServiceException(message);
        return null;
    }
    
    @Override
    public TransactionEntity commitTransaction(TransactionType transactionType, Integer amount) {
        if (transactionType != null && amount != null) {
            TransactionEntity transactionEntity = new TransactionEntity();
            transactionEntity.setType(transactionType);
            transactionEntity.setAmount(amount);
            transactionEntity.setEffectiveDate(new Date());
            transactionRepository.commitNewTransaction(transactionEntity);
            return transactionEntity;
        }
        String message = "Transaction Type, Amount cannot be null.";
        invokeServiceException(message);
        return null;
    }
    
    private void invokeServiceException(String message) {
        logger.error(message);
        throw new TransactionServiceException(message);
    }
    
}
