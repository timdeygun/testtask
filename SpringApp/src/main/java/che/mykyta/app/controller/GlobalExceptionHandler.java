package che.mykyta.app.controller;

import che.mykyta.app.service.exception.AccountServiceException;
import che.mykyta.app.service.exception.TransactionServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    
    @ExceptionHandler(AccountServiceException.class)
    public String handleAccountException(HttpServletRequest request) {
        //needed little more time to complete :D
        return "/";
    }
    
    @ExceptionHandler(TransactionServiceException.class)
    public String handleTransactionException(HttpServletRequest request) {
        //needed little more time to complete :D
        return "/";
    }
    
}
