package che.mykyta.app.controller;

import che.mykyta.app.controller.exception.AccountControllerException;
import che.mykyta.app.controller.request.TransactionRequestBody;
import che.mykyta.app.data.dto.TransactionDto;
import che.mykyta.app.data.entity.impl.TransactionEntity;
import che.mykyta.app.data.entity.impl.TransactionType;
import che.mykyta.app.data.mapper.impl.TransactionMapper;
import che.mykyta.app.service.AccountService;
import che.mykyta.app.service.TransactionService;
import che.mykyta.app.service.impl.TransactionServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/account/", produces = "application/json")
public class IntegerAccountController {
    
    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    
    private AccountService<TransactionEntity, Integer> accountService;
    private TransactionService transactionService;
    private TransactionMapper transactionMapper;
    
    @Autowired
    public IntegerAccountController(AccountService<TransactionEntity, Integer> accountService,
                                    TransactionService transactionService,
                                    TransactionMapper transactionMapper) {
        this.accountService = accountService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }
    
    @GetMapping(path = "/history")
    public List<TransactionDto> fetchTransactionsHistory() {
        List<TransactionEntity> transactionEntities = transactionService.fetchTransactionHistory();
        List<TransactionDto> transactionDtos = new ArrayList<>();
        
        for (TransactionEntity transactionEntity : transactionEntities) {
            transactionDtos.add(transactionMapper.map(transactionEntity));
        }
        
        logger.debug("Fetch Transactions History");
        return transactionDtos;
    }
    
    @GetMapping(path = "/transaction")
    public TransactionDto getTransactionById(@RequestParam String transactionId) {
        TransactionEntity transactionEntity = transactionService.getTransactionById(transactionId);
        if (transactionEntity == null) {
            throw new AccountControllerException(String.format("Transaction with ID [%s] not found", transactionId));
        }
        logger.debug(String.format("Get Transaction by ID [%s]", transactionId));
        return transactionMapper.map(transactionEntity);
    }
    
    @PostMapping(path = "/operation")
    public TransactionDto operateWithAccount(@RequestBody TransactionRequestBody requestBody) {
        String typeDescription = requestBody.getType();
        
        TransactionType transactionType = TransactionType
                .getTransactionTypeForDescription(typeDescription.toUpperCase());
        Integer amount = requestBody.getAmount();
        TransactionEntity transactionEntity = accountService.modifyAccountValue(transactionType, amount);
        
        logger.debug(String.format("Commit transaction operation [%s]", typeDescription));
        return transactionMapper.map(transactionEntity);
    }
}
