package che.mykyta.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "che.mykyta.app")
public class ApplicationRunner {
    
    public static void main(String[] args) {
        SpringApplication.run(ApplicationRunner.class, args);
    }
    
}