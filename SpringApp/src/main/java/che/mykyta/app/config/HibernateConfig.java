package che.mykyta.app.config;

public final class HibernateConfig {
    
    public static final String ID_GENERATOR_NAME = "uuid";
    public static final String ID_GENERATION_STRATEGY = "uuid2";
    
}
